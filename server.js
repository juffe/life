'use strict';
const restify = require('restify');
const config = require('./config');

const port = process.env.PORT || 3000;

const server = restify.createServer({
  name: 'Life',
});

server.use(restify.queryParser());
server.use(restify.bodyParser());
require('./src/server/routes')(server, config.paths);
server.listen(port);

console.log(`Life server running at http:\/\/localhost:${port}`);
