'use strict';
const rollup = require('rollup-stream');
const gulp = require('gulp');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const sourcemaps = require('gulp-sourcemaps');
const plumber = require('gulp-plumber');
const flow = require('gulp-flowtype');
const babel = require('rollup-plugin-babel');
const commonjs = require('rollup-plugin-commonjs');
const resolve = require('rollup-plugin-node-resolve');
const builtins = require('rollup-plugin-node-builtins');
const sass = require('gulp-sass');

const config = {
  rollup: {
    entry: './src/app/index.js',
    sourceMap: 'inline',
    format: 'es',
    plugins: [
      commonjs(),
      builtins(),
      resolve({
        jsnext: true,
        main: true,
        browser: true,
      }),
      babel({
        exclude: 'node_modules/**',
        plugins: ['transform-flow-strip-types'],
        presets: ['es2015-rollup']
      })
    ]
  },
  flow: {
    beep: false
  },
  paths: {
    app: ['./src/app/**/*.js'],
    dist: './pub/dist',
    bin: './bin',
    styles: './src/app/styles/**/*.scss'
  }
};

gulp.task('flow', () =>
  gulp.src(config.paths.app)
    .pipe(plumber())
    .pipe(flow()));

gulp.task('build', () =>
  rollup(config.rollup)
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.paths.dist)));

gulp.task('watch', () =>
  gulp.watch(config.paths.app, ['build']));

gulp.task('styles', () =>
  gulp.src(config.paths.styles)
    .pipe(sass())
    .pipe(gulp.dest(config.paths.dist)));

gulp.task('default', ['styles', 'flow', 'build']);
