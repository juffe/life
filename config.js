'use strict';
const path = require('path');

module.exports = {
  paths: {
    storage: path.join(__dirname, 'storage '),
    pub: path.join(__dirname, 'pub'),
    patterns: path.join(__dirname, 'patterns')
  }
};
