'use strict';

const DEFAULT_RULE = {
  name: 'Life',
  birth: [3],
  survival: [2, 3]
};

const parseRle = str =>
  str.replace(/(\d+)(\w)/g, (m, n, c) =>
    new Array(parseInt(n, 10) + 1).join(c));

const encodeRule = rule =>
  `B${rule.birth.join('')}/S${rule.survival.join('')}`;

const decodeRule = rule => {
  const parts = rule.split('/');
  return {
    birth: parts[0] ? parts[0].substring(1).trim().split('').map(Number) : [],
    survival: parts[1] ? parts[1].substring(1).trim().split('').map(Number) : []
  };
}

/**
 * Decode a rle string into pattern object.
 * 
 * @param  {String} pattern
 * @return {Object}
 */
const decodePattern = pattern => {
  let description = '';
  let rule = DEFAULT_RULE;
  let name = '';
  let author = '';

  const lines = pattern.split('\n').filter(line => {
    if (line.charAt(0) === '#') {
      if (line.charAt(1) === 'N') {
        name = line.substring(2).trim();
      } else if (line.charAt(1) === 'C') {
        description += line.substring(2).trim() + '\n';
      } else if (line.charAt(1) === 'O') {
        author = line.substring(2).trim();
      }
      return false;
    }
    return true;
  });

  const firstLine = lines.shift();
  const params = {};

  firstLine.split(',').forEach(param => {
    const parts = param.trim().split('=');
    params[parts[0].trim()] = parts[1].trim();
  }); 

  if (params.rule) {
    rule = params.rule.toUpperCase();
  }
  const rows = lines.join('').split('$');
  const seed = [];
  
  let incrementY = 0;
  rows.forEach((row, y) => {
    parseRle(row).split('').forEach((c, x) => {
      if (c === 'o') {
        seed.push([x, y + incrementY]);
      }
    });
    const lastItem = row[row.length - 1];
    if (!isNaN(lastItem)) {
      incrementY += parseInt(lastItem) - 1;
    }
  });

  return {name, description, author, rule, seed};
}

const encodePattern = pattern => {
  const lines = [];
  if (pattern.name) {
    lines.push('#N ' + pattern.name);
  }
  if (pattern.author) {
    lines.push('#A ' + pattern.author);
  }
  if (pattern.rule) {
    encodeRule(pattern.rule);
  }
  return lines.join('\n');
}

module.exports = {
  encodePattern,
  decodePattern,
  encodeRule,
  decodeRule
};
