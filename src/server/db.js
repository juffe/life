'use strict';

const Datastore = require('nedb-promise');
const config = require('../../config');
const path = require('path');

const filename = path.join(config.paths.storage, 'patterns.db');
const db = new Datastore({filename, autoload: true});

module.exports = db;
