'use strict';
const restify = require('restify');
const decodePattern = require('./rle').decodePattern;
const db = require('./db');
const Promise = require('bluebird');

module.exports = (server, paths) => {
  /**
   * Get all patterns.
   */
  server.get('/api/patterns', (req, res) => {
    const params = {};
    if (req.query.rule) {
      params.rule = req.query.rule;
    }
    const cursor = db.cfind(params);
    if (req.query.limit) {
      cursor.limit(req.query.limit);
    }
    if (req.query.page) {
      cursor.skip(req.query.page);
    }
    return Promise
      .all([cursor.exec(), db.count(params)])
      .then(values =>
        res.send({patterns: values[0], total: values[1]}));
  });

  /**
   * Get pattern.
   */
  server.get('/api/pattern/:id', (req, res) => {
    return db.findOne({_id: req.params.id}).then(doc => res.send(doc));
  });

  /**
   * Create pattern.
   */
  server.post('/api/pattern', (req, res) => {
    const pattern = decodePattern(req.body.rle);
    return db.insert(pattern).then(doc => res.send(doc));
  });

  /**
   * Update pattern.
   */
  server.put('/api/pattern/:id', (req, res) => {
    const pattern = req.body;
    return db.update({_id: req.params.id}, pattern).then(num => res.send({updated: num}));
  });
  
  /**
   * Delete pattern.
   */
  server.del('/api/pattern/:id', (req, res) => {
    return db.remove({_id: req.params.id}).then(num => res.send({removed: num}));
  });

  /**
   * Serve static files.
   */
  server.get(/.*/, restify.serveStatic({
    directory: paths.pub,
    default: 'index.html'
  }));

  return server;
}


