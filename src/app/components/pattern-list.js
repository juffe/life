import m from 'mithril'

export default {
  view(ctrl, args) {
    const {patterns = []} = args;
    return m('ul.pattern-list', patterns.map(pattern =>
      m('li.pattern-item', {
        class: args.selected === pattern._id ? 'selected' : '',
        onclick: args.onselect.bind(this, pattern)
      }, pattern.name)
    ))
  }
}
