'use strict'
import m from 'mithril';
import Life from '../life';
import {decodeRule, encodeRule} from '../life/rule';
import patternEditor from './pattern-editor';
import patternList from './pattern-list';
import Pattern from '../models/pattern';

const appendDom = (dom) => (element, initialized) => {
  if (!initialized) {
    element.appendChild(dom);
  }
}

const app = {}

app.vm = {
  init() {
    const canvasWidth = m.prop(600);
    const canvasHeight = m.prop(400);
    this.canvasHeight = canvasHeight;
    this.canvasWidth = canvasWidth;
    this.canvas = document.createElement('canvas');
    this.canvas.width = canvasWidth();
    this.canvas.height = canvasHeight();
    this.pattern = m.prop({});
    this.patterns = Pattern.list();
    this.editorOpen = m.prop(false);
    return this;
  }
}

app.controller = function(args) {
  this.vm = app.vm.init()
  this.life = new Life(this.vm.canvas)
  this.vm.patterns.then(patterns => this.setPattern(patterns[0]))

  this.action = (actionName) => () => {
    if (this.life && typeof this.life[actionName] === 'function') {
      this.life[actionName].call(this.life);
    }
  }

  this.refreshPatterns = () =>
    Pattern.list().then(this.vm.patterns);

  this.setPattern = (pattern) => {
    pattern = Object.assign({}, pattern);
    if (typeof pattern.rule === 'string') {
      pattern.rule = decodeRule(pattern.rule);
    }
    this.vm.pattern(pattern);
    return this;
  }

  this.addPattern = (rle) =>
    Pattern.create(rle).then(this.refreshPatterns.bind(this));

  this.openEditor = () => {
    m.redraw.strategy('diff');
    this.vm.editorOpen(true);
    return this;
  }

  this.updatePattern = () => {
    const pattern = this.vm.pattern();
    if (pattern._id && pattern._id !== this.life.pattern._id) {
      this.life.setPattern(pattern);
    }
  }
}

app.view = (ctrl, args) => {
  const {vm} = ctrl;
  const width = vm.canvasWidth;
  const height = vm.canvasHeight;
  const pattern = vm.pattern();
  return [
    m('div.left', {config: ctrl.updatePattern.bind(ctrl)}, [
      m('div.game', {config: appendDom(vm.canvas)}),
      m('div', [
        m('p', pattern.name || ''),
        m('p', pattern.description || ''),
        m('p', 'Rule: ' + pattern.rule ? encodeRule(pattern.rule) : ''),
      ]),
      m('div', [
        m('input', {
          type: 'text',
          value: width(),
          onchange: m.withAttr('value', width)
        }),
        m('span', 'x'),
        m('input', {
          type: 'text',
          value: height(),
          onchange: m.withAttr('value',  height)
        })
      ]),
      m('div.toolbar', [
        m('button', {onclick: ctrl.action('start')}, 'Start'),
        m('button', {onclick: ctrl.action('stop')}, 'Stop'),
        m('button', {onclick: ctrl.action('nextTick')}, 'Step'),
        m('button', {onclick: ctrl.action('reset')}, 'Reset')
      ])
    ]),
    m('div.right', [
      m.component(patternEditor, {
        onadd: ctrl.addPattern.bind(ctrl),
        open: vm.editorOpen()
      }),
      m.component(patternList, {
        patterns: vm.patterns(),
        onselect: ctrl.setPattern.bind(ctrl),
        selected: pattern._id
      }),
      m('button', {onclick: ctrl.openEditor.bind(ctrl)}, 'Add new')
    ])
  ];
}

export default app