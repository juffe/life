import m from 'mithril'

export default {
  controller(args) {
    this.rle = m.prop('')
    this.save = () => {
      if (args.onadd && this.rle()) {
        args.onadd.call(this, this.rle());
      }
    }
  },

  view(ctrl, args) {
    const params = {};
    if (args.open) {
      params.class = 'open';
    }
    return m('div.pattern-editor', params, [
      m('label', [
        m('span', 'Import from RLE'),
        m('br'),
        m('textarea', {
          cols: 60,
          rows: 30,
          value: ctrl.rle(),
          onchange: m.withAttr('value', ctrl.rle)
        })
      ]),
      m('button', {onclick: ctrl.save.bind(this)}, 'Add')
    ]);
  }
}
