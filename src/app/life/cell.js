// @flow
'use strict'
export const STATE_DEAD = 0;
export const STATE_ALIVE = 1;
export type State = 0 | 1;
export type Flag = 'kill' | 'revive';

export default class Cell {
  x: number;
  y: number;
  ghost: boolean;
  age: number;
  flag: ?Flag;
  state: State;

  constructor(x: number, y: number, alive: boolean = false) {
    this.x = x;
    this.y = y;
    this.state = alive ? 1 : 0;
    this.age = 0;
    this.ghost = false;
  }

  isAlive() {
    return this.state === STATE_ALIVE;
  }

  lives() {
    this.age++;
  }
  
  kill() {
    this.state = STATE_DEAD;
    this.ghost = true;
    this.age = 0;
  }

  revive() {
    this.state = STATE_ALIVE;
  }

  clearFlag() {
    if (this.flag === 'kill') {
      this.kill();
    } else if (this.flag === 'revive') {
      this.revive();
    }
    this.flag = null;
  }
}
