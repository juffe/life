// @flow
'use strict';
import range from 'lodash/range';
import pull from 'lodash/pull';
import Cell from './cell';
import type {Seed} from './pattern';

type Bounds = {
  x: [number, number],
  y: [number, number]
};

type Range = {
  x: number[],
  y: number[]
};

const rotateArray = (array: any[], times: number) => {
  const rotated = array.slice()
  const step = 1;
  const len = array.length - 1;
  let offset = 0;
  for (let i = 0; i <= len; i++) {
    if (i + step <= len) {
      rotated[i + step] = rotated[i];
      offset++;
    } else {
      rotated[i - offset] = rotated[i];
    }
  }
  return rotated;
}

const getRange = (bounds: Bounds): Range => {
  return  {
    x: range(bounds.x[0], bounds.x[1] + 1),
    y: range(bounds.y[0], bounds.y[1] + 1)
  };
}

const getBounds = (seed: Seed): Bounds  => {
  const x = seed.map(point => point[0]);
  const y = seed.map(point => point[1]);
  return {
    x: [
      Math.min.apply(Math, x),
      Math.max.apply(Math, x)
    ],
    y: [
      Math.min.apply(Math, y),
      Math.max.apply(Math, y)
    ]
  };
}

export default class World {
  height: number;
  width: number;
  grid: Array<Cell[]>;

  constructor(seed: Seed, width: number, height: number): void {
    this.grid = [];
    this.setSize(width, height);
    this.populate(seed);
  }

  /**
   * Populate world with the given seed. 
   */
  populate(seed: Seed | Function): World {
    this.grid = []
    const applySeed = typeof seed === 'function' ? seed : this.applySeed(seed);
    for (let x = 0; x < this.width; x++) {
      this.grid[x] = [];
      for (let y = 0; y < this.height; y++) {
        const isAlive = applySeed(x, y);
        this.grid[x].push(new Cell(x, y, isAlive));   
      }
    }
    return this;
  }

  /**
   * Returrns a function that applies the given seed to a single point. 
   */
  applySeed(seed: Seed): Function {
    const bounds = getBounds(seed);  
    const offset = [
      Math.floor((this.width - bounds.x[1] - bounds.x[0]) / 2),
      Math.floor((this.height - bounds.y[1] - bounds.x[0]) / 2)
    ];
    return (x: number, y: number): boolean => {
      for (let i = 0; i < seed.length; i++) {
        const point = seed[i];
        if (point[0] + offset[0] === x && point[1] + offset[1] === y) {
          return true;
        }
      }
      return false;
    }
  }

  fill(): World {
    return this.populate((x: number, y: number) => this.grid[x][y].isAlive());
  }

  setSize(width: number, height: number): World {
    this.width = width;
    this.height = height;
    return this;
  }

  /**
   * Count the number of living cells adjecent to the given cell. 
   */
  countLivingNeighbors(cell: Cell): number {
    const neighbors = this.getNeighbors(cell);
    return neighbors.filter(cell => cell.isAlive()).length;
  }

  /**
   * Get the cells adfejent to the given cell.  
   */
  getNeighbors(cell: Cell): Cell[] {
    const bounds = {
      x: [
        cell.x - 1 < 0 ? cell.x : cell.x - 1,
        cell.x + 1 > this.width ? cell.x : cell.x + 1
      ],
      y: [
        cell.y - 1 < 0 ? cell.y : cell.y - 1,
        cell.y + 1 > this.height ? cell.y : cell.y + 1
      ]
    };
    return pull(this.getCells(bounds), cell);
  }

  /**
   * Apply a callback function to each cell in the grid.
   */
  walk(callback: Function, bounds: ?Bounds) {
    const area = getRange(bounds || {
      x: [0, this.width],
      y: [0, this.height]
    });
    for (let x = 0; x < area.x.length; x++) {
      for (let y = 0; y < area.y.length; y++) {
        const row = this.grid[area.x[x]]
        if (row && row[area.y[y]]) {
          callback(row[area.y[y]]);
        }
      }
    }
  }

  move(x: number, y: number) {
    if (y !== 0) {
      this.grid = rotateArray(this.grid, y);
    }
    if (x !== 0) {
      for (let i = 0; i < this.width; i++) {
        this.grid = rotateArray(this.grid[i], x);
      }
    }
  }

  /**
   * Get a flat array of all the cells in the grid.
   */
  getCells(bounds: ?Bounds): Cell[] {
    const cells = [];
    this.walk(cells.push.bind(cells), bounds);
    return cells;
  }

  /**
   * Get a single cell in given coordinates.
   */
  getCell(x: number, y: number): ?Cell {
    if (this.grid[x] && this.grid[x][y]) {
      return this.grid[x][y];
    }
    return null;
  }
}