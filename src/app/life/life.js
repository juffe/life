// @flow
'use strict'
import World from './world';
import Cell from './cell';
import Renderer from './renderer';
import {EventEmitter} from 'events';
import {GUN} from './pattern';
import type {Pattern} from './pattern';

type GameOptions = {
  rendererConfig: Object,
  speed: number,
  pattern: Pattern
};

export default class Life extends EventEmitter {
  world: World;
  renderer: Renderer;
  canvas: HTMLCanvasElement;
  pattern: Pattern;
  generation: number;
  speed: number;
  running: boolean;
  handle: number;

  constructor(canvas: HTMLCanvasElement, {
    rendererConfig = {},
    speed = 100,
    pattern = GUN
  }: GameOptions = {}) {
    super();
    this.pattern = pattern;
    this.canvas = canvas;
    this.renderer = new Renderer(this.canvas, rendererConfig || {});
    this.world = this.createWorld();
    this.generation = 0;
    this.running = false;
    this.speed = speed;
    this.update();
  }

  createWorld(): World {
    return new World(this.pattern.seed, ...this.getDimensions());
  }

  resize() {
    this.world.setSize(...this.getDimensions()).fill();
    this.update();
  }
  
  zoom(value: number) {
    this.renderer.cellWidth += value;
    this.renderer.cellHeight += value;
    this.update();
  }

  getDimensions(): number[] {
    return [
      Math.floor(this.canvas.width / this.renderer.cellWidth), 
      Math.floor(this.canvas.height / this.renderer.cellHeight)
    ];
  }

  setPattern(pattern: Pattern) {
    this.pattern = pattern;
    this.reset();
  }

  move(x: number, y: number) {
    this.world.move(x, y);
    this.update();
  }

  nextTick() {
    this.emit('beforeUpdate', this);
    this.world.walk(cell => this.applyRule(cell));
    this.update();
    this.generation++;
    this.emit('afterUpdate', this);
  }

  applyRule(cell: Cell) {
    const {rule} = this.pattern
    const livingNeighbors = this.world.countLivingNeighbors(cell)
    if (cell.isAlive()) {
      if (rule.survival.indexOf(livingNeighbors) > -1) {
        cell.lives()
      } else {
        cell.flag = 'kill';
      }
    } else if (rule.birth.indexOf(livingNeighbors) > -1) {
      cell.flag = 'revive';
    }
  }

  update() {
    this.renderer.render(this.world);
  }

  start() {
    if (!this.running) {      
      this.emit('start');
      this.handle = setInterval(this.nextTick.bind(this), this.speed);
      this.running = true;
    }
  }

  stop() {
    if (this.running) {
      this.emit('stop');
      clearInterval(this.handle);
      this.running = false;
    }
  }

  reset() {
    this.stop();
    setTimeout(() => {
      this.world.populate(this.pattern.seed);
      this.generation = 0;
      this.update();
    }, 10);
  }
}
