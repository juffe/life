// @flow
'use strict'

export type Rule = {
  name: ?string,
  birth: [number],
  survival: [number]
};

export const encodeRule = (rule: Rule): string =>
  `B${rule.birth.join('')}/S${rule.survival.join('')}`;

export const decodeRule = (rule: string): Rule => {
  const parts = rule.split('/');
  parts[0] = parts[0].trim().replace('B', '');
  parts[1] = parts[1].trim().replace('S', '');
  return {
    name: '',
    birth: parts[0] ? parts[0].split('').map(Number) : [],
    survival: parts[1] ? parts[1].split('').map(Number) : []
  };
}
