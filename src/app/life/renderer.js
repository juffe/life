// @flow
'use strict'
import Cell from './cell';
import World from './world';

export type Color = [number, number, number];

export default class Renderer {
  canvas: HTMLCanvasElement;
  ctx: ?CanvasRenderingContext2D;
  cellWidth: number;
  cellHeight: number;
  colors: {alive: Color, dead: Color};

  constructor(canvas: HTMLCanvasElement, {
    cellWidth = 4,
    cellHeight = 4,
    colors = {
      alive: [0, 255, 0],
      dead: [0, 0, 0]
    },
  }: Object = {}) {
    this.canvas = canvas;
    this.ctx = this.canvas.getContext('2d');
    this.cellWidth = cellWidth;
    this.cellHeight = cellHeight;
    this.colors = colors;
  }

  render(world: World): ?CanvasRenderingContext2D {
    const {width, height} = this.canvas;
    const ctx = this.ctx;
    if (ctx) {
      const imageData = ctx.createImageData(width, height);
      world.walk(cell => this.renderCell(cell, imageData));
      ctx.putImageData(imageData, 0, 0);
    }
    return ctx;
  }

  renderCell(cell: Cell, imageData: ImageData) {
    cell.clearFlag();
    const rgb = this.getCellColor(cell);
    const data = imageData.data;
    const rowLength = data.length / imageData.height;
    const offset = (cell.y * rowLength * this.cellHeight) + (cell.x * 4 * this.cellWidth);
    for (let y = 0; y < this.cellHeight; y++) {
      let idx = (offset + y * rowLength) - 1;
      for (let x = 0; x < this.cellWidth; x++) {
        data[++idx] = rgb[0];
        data[++idx] = rgb[1];
        data[++idx] = rgb[2];
        data[++idx] = 255;
      }
    }
  }

  getCellColor(cell: Cell): Color {
    const {alive, dead} = this.colors;
    return cell.isAlive() ? alive : dead;
  }
}
