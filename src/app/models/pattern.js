import m from 'mithril'

export default {
  list() {
    console.log('list');
    return m
      .request({method: 'GET', url: '/api/patterns'})
      .then(data => data.patterns).then(m.prop([]));
  },
  
  create(rle) {
    const data = new FormData();
    data.append('rle', rle);
    return m.request({
      method: 'POST',
      url: '/api/pattern',
      data,
      serialize: data => data
    });
  }
}