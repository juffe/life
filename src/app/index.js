'use strict'
import m from 'mithril'
import app from './components/app'

m.mount(document.body, app)